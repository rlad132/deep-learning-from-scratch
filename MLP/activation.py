import numpy as np

class Sigmoid:
    
    "Define the sigmoid activation function and its derivative."
    
    
    def activate(self, z):
        "Compute the value of the sigmoid function at the point z."
        
        return 1./(1. + np.exp(-z))
    
    def derivative(self, z):
        "Compute the derivative of the sigmoid function."
        
        return self.activate(z)*(1. - self.activate(z))
 
    
class Tanh:
    
    'Define the Tanh activation function and its derivative.'
    
    def activate(self, z):
        pass
    
    def derivative(self, z):
        pass


class ReLU:
    
    def activate(self, z):
        
        return np.maximum(0, z)
    
    def derivative(self, z):
        
        
        return np.where(z<0, 0, 1)